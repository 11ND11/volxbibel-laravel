<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Book;
use App\Models\Chapter;
use App\Models\Verse;
use File;
use Illuminate\Support\Facades\DB;

class BibleSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $dataFile = getenv('APP_PATH') . "googleApi/data.json";
        $LutBookOrderFile = getenv('APP_PATH') . "database/seeders/data/lut_book_order.json";

        if (file_exists($dataFile)) {
            echo "begin to read and decode json..." . PHP_EOL;

            $dataJson = File::get($dataFile);
            $data = json_decode($dataJson);
            $lutBookOrderJson = File::get($LutBookOrderFile);
            $lutBookOrder = json_decode($lutBookOrderJson);
            echo "done" . PHP_EOL;
            $chapterId = 0;
            $verses = [];
            $books = [];
            $chapters = [];

            foreach ($data as $document) {
                $documentId = $document->documentId;

                echo $document->title . PHP_EOL;

                $titleArray = explode(" ", $document->title);
                $titleArrayLastValue = $titleArray[count($titleArray) - 1];
                // remove 'VOLXBIBEL' and '-' from title
                unset($titleArray[0], $titleArray[1]);
                // check, if the last item of title is a number
                if (is_numeric($titleArrayLastValue)) {
                    $chapterNumber = intval(array_pop($titleArray));
                } else {
                    $chapterNumber = 0;
                    // if "Whatsapp an Phil" oder "Mail an Judas"
                    if ($documentId === '13K3PB2mK_-mTkgtkTJxx9na8NGN0SOhzd4ROKw_SVX0' || $documentId === '1GHHcVsdNUOGEFGc3_2ya3Zy4Q_OUKCghB403iN7coIk') {
                        $chapterNumber = 1;
                    }
                }
                $bookTitle = implode(" ", $titleArray);

                // prepare book items
                // possible title values: 2. Korinther, Galater, 1.Timotheus, WhatsApp an Phil
                // @Todo: save lowercase in database?
                $booksIndex = array_search(trim($bookTitle), array_column($books, 'title'));

                if(!($booksIndex || $booksIndex === 0)) {
                    $filteredBookTitle = strtolower(str_replace(' ', '', $bookTitle));
                    $bookId = $lutBookOrder->books->{$filteredBookTitle};

                    array_push($books,[
                        "id" => $bookId,
                        "title" => trim($bookTitle),
                        "testament" => $document->testament
                    ]);

                    // prepare chapter items
                    $chapterId++;
                    array_push($chapters,[
                        "id" => $chapterId,
                        "google_uid" => $documentId,
                        "number" => $chapterNumber,
                        "book_id" => $bookId
                    ]);
                } else {
                    // prepare chapter items
                    $chapterId++;
                    array_push($chapters,[
                        "id" => $chapterId,
                        "google_uid" => $documentId,
                        "number" => $chapterNumber,
                        "book_id" => $books[$booksIndex]['id']
                    ]);
                }

                // prepare content of verses
                foreach ($document->content as $contentItem) {
                    $text = '';

                    foreach ($contentItem->text as $index => $textItem) {
                        if (count($contentItem->text) === 1) {
                            array_push($verses, ['chapter_id' => $chapterId, 'label' => 'headline', 'text' => $textItem, 'pos' => $contentItem->position]);
                        } else {
                            $textItemTrimmed = trim($textItem);

                            if (is_numeric($textItemTrimmed)) {
                                $label = $textItemTrimmed;
                            } else {
                                // preg_match('/\d{1,2}[\-\/]\d{1,2}/'
                                if(strlen($textItemTrimmed) < 6 && preg_match('/\d{1,2}[\-\/]\d{1,2}/', $textItemTrimmed, $matches)) {
                                    $label = $textItemTrimmed;
                                    //echo $document->title . PHP_EOL;
                                    //var_dump($matches);
                                } else {
                                    $text .= $textItem;
                                }
                            }

                            if ($index === count($contentItem->text) - 1 ) {
                                array_push($verses, ['chapter_id' => $chapterId, 'label' => $label, 'text' => ltrim($text), 'pos' => $contentItem->position]);
                            }
                        }
                    }
                }
            }

            // @ToDo: validation
            // Find no ASCII: preg_match('/[^ -~]/'
            // Count 66 books
            // if error: no db actions and send error mail

            // fire DB actions
            echo 'Start truncate and insert data...' . PHP_EOL;
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');

            Verse::truncate();
            Chapter::truncate();
            Book::truncate();

            Book::insert($books);
            Chapter::insert($chapters);

            $chunks = array_chunk($verses, 5000);

            foreach ($chunks as $chunk) {
                Verse::insert($chunk);
            }

            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        } else {
            echo "ERROR - No file found. Please fetch data to use fetchDocumentsData.js " . PHP_EOL;
        }
    }
}
