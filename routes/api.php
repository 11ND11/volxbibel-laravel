<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\ChapterController;
use App\Http\Controllers\Api\BookController;
use App\Http\Controllers\Api\VerseController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->name('api.')->group(function() {

    // API example by Laravel
    // Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    //     return $request->user();
    // });

    //
    // Book
    //
    Route::name('books.')->group(function() {
        Route::get('books', [BookController::class, 'index'])->name('index');
    
    });

    //
    // Chapter
    //
    Route::name('chapters.')->group(function() {
        Route::get('{book}_{chapter}', [ChapterController::class, 'show'])->name('show');
    });

    //
    // Verse
    //
    Route::name('verses.')->group(function() {
        Route::get('{book}_{chapter}/{verse}', [VerseController::class, 'show'])->name('show');

    });

});