<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Web\BookController;
use App\Http\Controllers\Web\ChapterController;
use App\Http\Controllers\Web\VerseController;
use App\Http\Controllers\HomeController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/random', [ChapterController::class, 'showRandom'])->name('chapters.show.random');
Route::get('/spruch-des-tages', [ChapterController::class, 'showSpruchDesTages'])->name('chapters.show.spruchDesTages');

/*
 * Book routes
 */
Route::name('books.')->group(function() {
    // Route::get('books', [BookController::class, 'index'])->name('index');
    Route::get('/book/{book:title}', [BookController::class, 'show'])->name('show');
});

/*
 * Chapter routes
 */
Route::name('chapters.')->group(function() {
    Route::get('/book/{book:title}/chapter/{chapter:number}', [ChapterController::class, 'show'])->name('show');
});

/*
 * Verse routes
 */
Route::name('verses.')->group(function() {
    Route::get('/book/{book:title}/chapter/{chapter:number}/{verse:label}', [VerseController::class, 'show'])->name('show');
});

