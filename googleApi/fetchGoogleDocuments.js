const fs = require('fs');
const path = require('path');
const readline = require('readline');
const {google} = require('googleapis');
const startTime = new Date();
const requestBatchCount = 190
let batchIndexEndPerMinute = 0;
let isLastBatch = false;
let dataAll = [];
let apiErrors = 0;

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/documents.readonly'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = path.resolve(__dirname, './token.json');

// Load client secrets from a local file.
fs.readFile(path.resolve(__dirname, './credentials.json'), (err, content) => {
    if (err) {
        return console.log('Error loading client secret file:', err);
    }
    // Authorize a client with credentials, then call the Google Docs API.
    authorize(JSON.parse(content), writeDataFile);
});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
    const {client_secret, client_id, redirect_uris} = credentials.web;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) {
            return getNewToken(oAuth2Client, callback);
        }
        oAuth2Client.setCredentials(JSON.parse(token));
        callback(oAuth2Client);
    });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
        rl.close();
        oAuth2Client.getToken(code, (err, token) => {
            if (err) {
                return console.error('Error retrieving access token', err);
            }
            oAuth2Client.setCredentials(token);
            // Store the token to disk for later program executions
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                if (err) {
                    console.error(err);
                }
                console.log('Token stored to', TOKEN_PATH);
            });
            callback(oAuth2Client);
        });
    });
}

/**
 * get documents from Google and write json file
 * @param {google.auth.OAuth2} auth The authenticated Google OAuth 2.0 client.
 */
function writeDataFile(auth) {
    const docs = google.docs({version: 'v1', auth});
    const fs = require('fs');
    const documentIds = JSON.parse(fs.readFileSync(path.resolve(__dirname, './documentIds.json'), {
            encoding: 'utf8',
            flag: 'r'
        },
        function (err) {
            if (err) {
                console.log(err);
            }
        }))
        .ids;
    let batchIndex = batchIndexEndPerMinute === 0 ? batchIndexEndPerMinute : ++batchIndexEndPerMinute;
    batchIndexEndPerMinute = batchIndexEndPerMinute + requestBatchCount - 1;

    if (batchIndexEndPerMinute >= documentIds.length) {
        batchIndexEndPerMinute = documentIds.length - 1;
        isLastBatch = true;
    }

    console.log('batch index start:', batchIndex);
    console.log('batch index end:', batchIndexEndPerMinute);

    let promises = [];

    while (batchIndex <= batchIndexEndPerMinute) {
        promises.push(getDocumentsData(documentIds[batchIndex]));
        batchIndex ++;
    }

    async function getDocumentsData(documentId) {
        const p = new Promise((resolve, reject) => {
            docs.documents.get({
                documentId: documentId.id,
                suggestionsViewMode: 'PREVIEW_WITHOUT_SUGGESTIONS'
            }, (err, res) => {
                if (err) {
                    apiErrors++;
                    console.log('ERROR for document ' + documentId.id);
                    return console.log('The API returned an error: ' + err);
                }

                let elements = [];
                for (const content of res.data.body.content) {
                    let textBlocks = [];

                    if (content.paragraph) {
                        for (const element of content.paragraph.elements) {
                            if (element.textRun) {
                                textBlocks.push(element.textRun.content.replace('/\n/g', '').replace('/\u000b/g', '<br>'));
                            }
                        }

                        elements.push({
                            position: content.startIndex,
                            text: textBlocks,
                        });
                    }
                }

                const data = {
                    title: res.data.title.trim(),
                    content: elements,
                    revisionId: res.data.revisionId,
                    documentId: res.data.documentId,
                    testament: documentId.testament
                }

                console.log(JSON.stringify(data.title));
                resolve(data);
            });
        });
        return p;
    }

    // write file, if all requests are successfully done
    Promise.all(promises)
        .then((data) => {
            console.log('Fetched ' + data.length + ' GoogleDocs documents');

            // add batch data to dataAll array
            dataAll = dataAll.concat(data);

            if (isLastBatch) {
                console.log('Write ' + dataAll.length + ' GoogleDocs documents to data.json...');
                fs.writeFileSync(path.resolve(__dirname, 'data.json'), JSON.stringify(dataAll), function (err, result) {
                    if (err) {
                        console.log('error', err);
                    }
                });

                const duration = (new Date() - startTime) / 1000;
                console.log('Done. Got all data from Google - ' + (duration/60).toFixed(2) + 'min');
                if (apiErrors > 0) console.log(apiErrors + ' ERRORS during data fetch.');
            } else {
                console.log('Next batch is waiting (1 min) ...');

                setTimeout(function(){
                    writeDataFile(auth);
                }, 60000);
            }
        })
        .catch((err) => console.log(err));
}
