## How to seed json to database with laravel
Nice tutorial (here) [https://hdtuto.com/article/how-to-seed-with-json-file-in-laravel]

## Google API
JSON Example of a VOLXBIBEL GoogleDocs document: volxbibelDocsExample.json

### Usage Limits
https://developers.google.com/docs/api/limits

-> 3000 read requests per project per 60 seconds
