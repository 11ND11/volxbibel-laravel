/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Boot Inertia.js and create fresh laravel instance.
 * Additional we support inertia's progress @see https://laracasts.com/series/build-modern-laravel-apps-using-inertia-js/episodes/5
 */
 import { createApp, h } from 'vue';
 import { App, plugin } from '@inertiajs/inertia-vue3';

 import BaseLayout from './components/BaseLayout.vue';

 const el = document.getElementById('app');
 const app = createApp({
     render: () => h(App, {
         initialPage: JSON.parse(el.dataset.page),
         resolveComponent: name => require(`./pages/${name}`).default
     })
 });

 app.component('base-layout', BaseLayout);

 app.config.globalProperties.$route = window.route
 app.provide('$route', window.route);
 app.use(plugin).mount(el);
