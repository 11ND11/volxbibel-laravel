## DIE VOLXBIBEL Web-App

Die Volxbibel geht in die X. Auflage, zusammen mit einer großartigen, ständig wachsenden Community. Unser Ziel: Gottes Wort den Menschen bekannt zu machen, die bisher nie einen Berührungspunkt mit der Bibel hatten. Leute in ihrem Leben, mit ihren Worten und ihrem Slang abzuholen, das ist die Mission der Volxbibel.

Das Voxbibel-Wiki hat uns treue Dienste geleistet. Hier haben wir Texte übersetzt, darüber gebetet, diskutiert und beraten. Außerdem waren die Texte direkt für die Leserschaft verfügbar. Doch nun ist es Zeit, etwas Neues zu schaffen. Eine neue App, die die Ausgabe der Volxbibel komfortabler macht, Bearbeitung als auch die Ausgabe richtig voneinander trennt und das Projekt auf den Stand der Zeit bringt.

## Projektstruktur 'App-Entwicklung'

### Ordnerstruktur

- **_ASSETS Ordner** Der _ASSETS Ordner beinhaltet Content, der für die Entwicklung relevant ist, aber nicht mit ausgerollt werden soll. Z.B. Grafiken, Notizen etc.
- **googleApi Ordner** Hier befinden sich alle benötigten dateien, um die Google API mit node.js anzusprechen (nicht im Git: credentials.json & token.json) und die aktuellen Versionen der Bücher, Kapitel sowie der diesen zugehörigen Verse in unsere Datenbank zu migrieren.

## Vorbereitung der Entwicklungsumgebung
Clonen des aktuellen Git-Repositories. Per Console in das "volxbibel_laravel" Verzeichnis (`% cd volxbibel_laravel`) und hier ein initiales `composer install`. Im Anschluss ist das Laravel-Framework ready to go.


### Datenbank
Wir nutzen Laravel Migrations, um eine aktuelle Struktur der Datenbank zu verteilen. Gefüllt wird die Datenbank während der Entwicklung durch Laravel Seeds.

## Lokale Entwicklung mit Sail
**Projekt startup**

```
// start docker containters with sail
sail up -d
// fetch VOLXBIBEL data from Google Docs
sail node googleApi/fetchGoogleDocuments.js
// prepare db
sail artisan migrate
// fill db with VOLXBIBEL data
sail artisan db:seed
// start npm for (vue)js stuff, otherwise you will get 404s
sail npm install
sail npm run watch
```

**local phpmyadmin container mit Sail** 
- url: localhost:8080
- user: sail
- pw: password

## In case of emergency - Tipps, wenn Sail nicht mehr will
- Versuche folgende Schritte
```
sail down -v
sail artisan config:cache
sail artisan route:clear
sail artisan migrate
```

- Gern vergessen und gewundert, warum keine Ausgabe zu sehen ist: `sail npm run watch`

# To-Dos und Meilensteine
## Allgemein
### Parsen/Anzeigen der Texte
- [x] ß -> ss in headlines
- [ ] '/n' raus oder ordentlich auswerten?
- [x] Versenummern "1-3" oder "12/13" erkennen
- [ ] wenn keine Versnummer erkannt wird, dann auch keinen Vers im Text ausgeben
- [x] Lizenzinfo anzeigen
- [ ] Besonderheiten in Kapitel 0
- [ ] Verlinkungen mit übernehmen (1. Mose Intro)
- [ ] falsche Überschrift: 2. MOSE 15
- [x] Intro evtl raus aus der Liste wegen hässlicher formatierung und unschöner Einstieg?
- [ ] Intro besser integrieren bzw anzeigen?
- [ ] Fußnoten: 1. Korinther 6,7,9;  Johannes 15,19; Apg 1,5,16, Römer 2,5,12
- [ ] Joh 15 und 19 Fußnoten
- [ ] Intro evtl raus aus der Liste wegen hässlicher formatierung und unschöner Einstieg?

### Routes
 - [x] Random Chapter
 - [ ] Top 20 with most comments and suggestions
 - [ ] Last changes

### FE
- ...

## API
### Abruf von Daten
- [ ] Abruf von kompletten Büchern
- [ ] Abruf von Kapiteln
- [ ] Abruf von Versen
- [ ] Vers des Tages
- [ ] Kapitel des Tages

### Security
- [ ] Zugangsbeschränkung
- [ ] Rate-Limiting

## Google
- [ ] Mitarbeiter hinzufügen über node und API lösen (inkl.Mailversand?)

# Sonstiges

Redirect Wiki -> Laravel App
```
RewriteCond %{HTTP_HOST} ^wiki\.volxbibel\.com
RewriteRule ^([^_]*)_([^_]*)$ https://lesen.volxbibel.de/book/$1/chapter/$2 [R=301,L]
RewriteRule ^/([^_]*)$ https://lesen.volxbibel.de/book/$1/ [R=301,L]
RewriteRule ^(.*)$ https://lesen.volxbibel.de/$1 [R=301,L]
```

# Deployment DF
```
git clone https://11ND11@bitbucket.org/11ND11/volxbibel-laravel.git
composer install --no-dev
/usr/bin/php80 artisan migrate
/usr/bin/php80 artisan db:seed
/usr/bin/php80 artisan optimize:clear
```
