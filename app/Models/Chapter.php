<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    /**
     * Chapter number.
     *
     * @var int
     */
    protected $number;

    /**
     * Get the book of the chapter.
     */
    public function book()
    {
        return $this->belongsTo(Book::class);
    }

    /**
     * Get the verses of the chapter.
     */
    public function verses()
    {
        return $this->hasMany(Verse::class);
    }
}
