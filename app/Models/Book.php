<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    /**
     * Book title.
     *
     * @var string
     */
    protected $title = '';

    /**
     * Get the chapters of the book.
     */
    public function chapters()
    {
        return $this->hasMany(Chapter::class);
    }

    public function verses()
    {
        return $this->hasManyThrough(Verse::class, Chapter::class);
    }
}
