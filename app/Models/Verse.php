<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Verse extends Model
{
    /**
     * Verse number.
     *
     * @var int
     */
    protected $number;

    /**
     * Get the chapter of the verse.
     */
    public function chapter()
    {
        return $this->belongsTo(Chapter::class);
    }
}
