<?php

//  sail php artisan compare

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Compare extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'compare';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @param
     * @return mixed
     */
    public function handle()
    {
        echo PHP_EOL . 'start read XML' . PHP_EOL;
        $filePath = "_ASSETS/wiki_druckversion_v4.xml";

        $textFilePath = "_ASSETS/";

        if (file_exists($filePath)) {
            $xml = simplexml_load_file($filePath);
            $book = "";
            $index = 0;

//            print_r($xml);
            foreach ($xml as $chapter) {
                $title = $chapter->title;
                $title = str_replace("ä", "ae", $title);
                $title = str_replace("ü", "ue", $title);
                $title = str_replace("ö", "oe", $title);
                $title = str_replace("Ä", "Ae", $title);
                $title = str_replace("Ü", "Ue", $title);
                $title = str_replace("Ö", "Oe", $title);
                $title = str_replace("ß", "ss", $title);
                $bookExplode = explode(" ", $title);

                echo $book . PHP_EOL;

                if ($index == 0) {
                    echo 'file: ' . $title . PHP_EOL;
                    $book = $bookExplode[0];
                    $chapterFile = fopen($textFilePath . $title . ".txt", "w") or die("Unable to open file!");
                    $txt = '';
                }

                if($book !== $bookExplode[0]) {
                    echo '!!! new file' . PHP_EOL;
                    fwrite($chapterFile, $txt);
                    fclose($chapterFile);
                    // open next file
                    $chapterFile = fopen($textFilePath . $title . ".txt", "w") or die("Unable to open file!");
                    $txt = preg_replace('/\s+/', '', $chapter->revision->text);
                } else {
                    echo 'same file' . PHP_EOL;
                    $txt .= preg_replace('/\s+/', '', $chapter->revision->text);
                }
                $book = $bookExplode[0];
                $index++;
            }
        } else {
            exit('Konnte Datei nicht öffnen.');
        }
    }
}
