<?php

namespace App\Http\Resources;

use App\Models\Chapter;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ChapterCollection;

class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $hasIntro = $this->chapters->contains('number', '0');

        return [
            'id' => $this->id,
            'title' => $this->title,
            'chapters' => new ChapterCollection($this->whenLoaded('chapters')),
            'hasIntro' => $hasIntro,
            'chaptersCount' => $hasIntro ? ($this->chapters->count() - 1) : $this->chapters->count()
        ];
    }
}
