<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\HttpException;

use App\Http\Resources\BookResource;
use App\Http\Resources\VerseCollection;

class ChapterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        try {
            $book = $this->whenLoaded('book');
            $verses = $this->whenLoaded('verses');

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            throw new HttpException(500, $e->getMessage());
        }  
        
        return [
            'id' => $this->id,
            'book' => new BookResource($this->whenLoaded('book')),
            'number' => $this->number,
            'verses' => new VerseCollection($this->whenLoaded('verses')),
            'guid' => $this->google_uid
        ];
    }
}
