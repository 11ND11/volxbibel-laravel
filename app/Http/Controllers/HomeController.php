<?php

namespace App\Http\Controllers;

use inertia\Inertia;

use App\Models\Book;
use App\Http\Resources\BookCollection;

/**
 * alle Bücher mit Namen und Kapitelanzahl inkl. Aller Kapitel mit Nummer und Google-Id
 */
class HomeController extends Controller
{

    /**
     * Show the application home page with Andy's wishes:
     * "alle Bücher mit Namen und Kapitelanzahl inkl. aller Kapitel mit Nummer und Google-Id"
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return Inertia::render('Home', [
            'books' => new BookCollection(Book::all()),
        ]);
    }
}
