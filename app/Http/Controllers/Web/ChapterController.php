<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use inertia\Inertia;

use App\Models\Book;

use App\Http\Resources\ChapterResource;
use App\Models\Chapter;

class ChapterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $book The book's title from URI (Route)
     * @param  int  $chapterNumber the chapter's number from URI (Route)
     * @return \Illuminate\Http\Response
     */
    public function show($bookTitle, $chapterNumber)
    {
        try {
            $book = Book::where('title', $bookTitle)->first();
            $chapter = new ChapterResource(Chapter::where('book_id', $book->id)->where('number', $chapterNumber)->with(['book', 'verses'])->first());
        } catch(\Exception $exception) {
            // return back()->withError($exception->getMessage())->withInput();
            return back()->withError("Fehler! Keine Bibelstelle zu " . $bookTitle . ":" . $chapterNumber . " gefunden.");
        }
        return Inertia::render('Chapter', [
            'chapter' => $chapter,
        ]);
    }

    /**
     * Display a random resource.
     *
     * @param  String  $book The book's title from URI (Route)
     * @return \Illuminate\Http\Response
     */
    public function showRandom()
    {
        $book = Book::all()->random();
        return Inertia::render('Random', [
           'chapter' => new ChapterResource(Chapter::where('book_id', $book->id)->with(['book', 'verses'])->get()->random()),
        ]);

    }

    /**
     * Display a random resource.
     *
     * @param  String  $book The book's title from URI (Route)
     * @return \Illuminate\Http\Response
     */
    public function showSpruchDesTages()
    {
        $book = Book::all()->random();
        return Inertia::render('SpruchDesTages', [
            'chapter' => new ChapterResource(Chapter::where('book_id', $book->id)->with(['book', 'verses'])->get()->random()),
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
