<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Book;
use App\Models\Chapter;
use App\Http\Resources\ChapterResource;

class ChapterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $book The book's name from URI (Route)
     * @param  int  $chapter_number the chapter's id from URI (Route)
     * @return \Illuminate\Http\Response
     */
    public function show($book, $chapter)
    {
        $book = Book::where('title', $book)->first();

        return new ChapterResource(Chapter::where('book_id', $book->id)->where('number', $chapter)->first());

        // As response
        // return response()->json(['chapter' => new ChapterResource(Chapter::where('book_id', $book->id)->where('number', $chapter)->first())], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
